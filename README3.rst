============================
CentOS6へのredmineインストール
============================

Redmine のインストール場所 : /var/lib/redmine
===========================================

シンボリックリンク : /var/www/html/redmine => /var/lib/redmine/public
====================================================================


http://blog.redmine.jp/tags/インストール・設定/
http://qiita.com/mozo/items/6ec0151dad350c051399
http://oki2a24.com/2014/02/08/try-to-install-ruby-and-redmine/

環境
====
項目	内容（バージョン/パス等）	内容（バージョン/パス等）
Vagrant

Cent OS	6.5	同左
redmine	2.5.1.stable	2.5.2.stable
ruby	2.0.0-p481	同左
gem	2.3.0	2.4.1
rails	3.2.17	3.2.19
environment	production	同左
Database adapter	Mysql2	同左
アプリディレクトリ	/var/lib/redmine/ 同左
httpdサブディレクトリ	/var/www/html/redmine ※シンボリックリンク	同左
URL	http://[hostname]/redmine	同左

インストール手順
==============

0. ネットワーク関連
=================
iptables、ip6tables、selinuxなどは必要に応じて設定。

1. yumパッケージ
================

1.1. パッケージインストール
========================
# yum install gcc gcc-c++
# yum install httpd httpd-devel
# yum install mysql-server mysql-devel
# yum install openssl-devel readline-devel zlib-devel curl-devel

1.2. epelパッケージインストール
============================

epelリポジトリ追加
================

# yum install epel-release
# yum install libyaml libyaml-devel --enablerepo=epel
# yum install nodejs npm --enablerepo=epel

3. ruby
=======
3.1. rubyインストール
====================

実際はrbenvやruby-build使うかもしれませんが割愛。
以下からrubyソースパッケージをダウンロード
https://www.ruby-lang.org/ja/downloads/

ビルド＆インストール
=================
# tar zxvf ruby-2.0.0-p481.tar.gz
# cd ruby-2.0.0-p481
# ./configure --disable-install-doc
# make
# make install

確認
# ruby -v
ruby 2.0.0p481 (2014-05-08 revision 45883) [x86_64-linux]

3.2. gemパッケージインストール
===========================

gemを最新版に更新
===============
# gem update --system --no-rdoc --no-ri
# gem -v
2.3.0

bundlerインストール
===================
# gem install bundler --no-rdoc --no-ri

passenger関連gemインストール
==========================
# gem install daemon_controller rack passenger --no-rdoc --no-ri

インストールされたgemパッケージ確認
===============================
# gem list
bigdecimal (1.2.0)
bundler (1.6.2)
daemon_controller (1.2.0)
io-console (0.4.2)
json (1.7.7)
minitest (4.3.2)
passenger (4.0.45)
psych (2.0.0)
rack (1.5.2)
rake (0.9.6)
rdoc (4.0.0)
rubygems-update (2.3.0)
test-unit (2.0.0.0)

4. mysql
========
4.1. my.cnfにutf8の設定を追加
===========================

/etc/my.cnf
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
user=mysql
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

character-set-server=utf8

[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

[mysql]
default-character-set=utf8

4.2. mysqlサービス設定
=====================

起動
====
# service mysqld start

自動起動設定
===========
# chkconfig mysqld on

セキュリティ設定（rootパスワード他）
# mysql_secure_installation

DB、ユーザー作成
===============
#
mysql> create database redmine_db character set utf8;
mysql> create user 'redmine_user'@'localhost' identified by 'redmine_pass';
mysql> grant all privileges on redmine_db.* to 'redmine_user'@'localhost';
mysql> flush privileges;
mysql> exit

5. redmine
==========
5.1. redmineリソース配置
======================

以下から最新のredmineを取得する
============================
git clone https://github.com/redmine/redmine.git /var/lib/redmine

# Redmine をインストールしたディレクトリに移動
cd /var/lib/redmine/
# Redmine データベース設定例ファイルをコピー
# データベース設定ファイルを編集
vim config/database.yml

config/database.yml
===================
production:
  adapter: mysql2
  database: redmine_db
  host: localhost
  username: redmine_user
  password: redmine_pass
  encoding: utf8



5.2. redmineビルド（bundlerを使用）
================================

5.2.2. database.ymlを作成
=========================

config/database.yml
  production:
  adapter: mysql2
  database: db_redmine
  host: localhost
  username: user_redmine
  password: "********"
  encoding: utf8

5.2.3. bundleインストール（インターネット上の最新リソースを参照）
=========================================================

"vendor/bundle"にgemパッケージ等をインストールする
=============================================
対象はproduction環境のみ、rmagickも除く
# bundle install --without development test rmagick --path vendor/bundle

5.2.4. redmineのビルド
=====================

# bundle exec rake generate_secret_token
# RAILS_ENV=production bundle exec rake db:migrate



6. passengerとhttpdの設定
========================

6.1. httpdモジュールインストール
=============================

インストール
==========
# gem install passenger --no-rdoc --no-ri
# rbenv rehash
# passenger-install-apache2-module

6.2. passenger用conf設定
========================

# sudo vim /etc/httpd/conf.d/passenger.conf

/etc/httpd/conf.d/passenger.conf

LoadModule passenger_module /home/vagrant/.rbenv/versions/2.3.0/lib/ruby/gems/2.3.0/gems/passenger-5.0.28/buildout/apache2/mod_passenger.so
   <IfModule mod_passenger.c>
     PassengerRoot /home/vagrant/.rbenv/versions/2.3.0/lib/ruby/gems/2.3.0/gems/passenger-5.0.28
     PassengerDefaultRuby /home/vagrant/.rbenv/versions/2.3.0/bin/ruby
   </IfModule>

# Passengerが追加するHTTPヘッダを削除するための設定（任意）。
Header always unset "X-Powered-By"
Header always unset "X-Rack-Cache"
Header always unset "X-Content-Digest"
Header always unset "X-Runtime"

PassengerMaxPoolSize 20
PassengerMaxInstancesPerApp 4
PassengerPoolIdleTime 3600
PassengerHighPerformance on
PassengerStatThrottleRate 10
PassengerSpawnMethod smart
RailsAppSpawnerIdleTime 86400
PassengerMaxPreloaderIdleTime 0

# DocumentRootのサブディレクトリで実行する設定
RackBaseURI /redmine



6.3. シンボリックリンク作成
========================
# /var/www/html/ に 先にredmine/ を作成するということではない。
# /var/www/html/ に シンボリックリンク redmineを作成するということ

# ln -s /var/lib/redmine/public /var/www/html/redmine

6.4. 権限設定
============

# chown -R apache:apache /var/www/html/redmine
# chown -R apache:apache /var/lib/redmine


6.5. httpdサービスの再起動
========================
# service httpd configtest
# /etc/init.d/httpd restart

6.6 cssファイルの反映
====================
var/lib/redmine/public/htaccess.fcgi.exampleを
var/lib/redmine/public/.htaccessに rename
cp /var/lib/redmine/public/htaccess.fcgi.example /var/lib/redmine/public/.htaccess

# これで Redmine のインストールが終了です！